<?php

namespace App\Security\Exception;

use Symfony\Component\Security\Core\Exception\RuntimeException;

/**
 * CustomerFilterDisabledException is thrown if the customer_filter isn't enabled but should be.
 *
 * @author Sébastien Blanc <s.blanc@ubitransport.com>
 */
class CustomerFilterDisabledException extends RuntimeException
{
}
