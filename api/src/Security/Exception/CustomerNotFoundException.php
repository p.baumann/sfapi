<?php

namespace App\Security\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * UserNotFoundException is thrown if a User cannot be found.
 *
 * @author Sébastien Blanc <s.blanc@ubitransport.com>
 */
class CustomerNotFoundException extends AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'No user could be found.';
    }
}
