<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200313121407 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE brands (id UUID NOT NULL, legacy_id INT NOT NULL, nom_abrege VARCHAR(255) NOT NULL, tag VARCHAR(255) NOT NULL, utiliser_nom_marital VARCHAR(255) NOT NULL, taux_commission VARCHAR(255) NOT NULL, couleur_hex_theme VARCHAR(255) NOT NULL, couleur_hex_main VARCHAR(255) NOT NULL, couleur_hex_main_fonce VARCHAR(255) NOT NULL, autoriser_devis_online VARCHAR(255) NOT NULL, est_actif VARCHAR(255) NOT NULL, has_extra_fields BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN brands.id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE brands');
    }
}
