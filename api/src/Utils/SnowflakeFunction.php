<?php

namespace App\Utils;

use Godruoyi\Snowflake\Snowflake;

/**
 * Class SnowflakeFunction
 * This static class is to generate an ID based on the actual microtime.
 */
class SnowflakeFunction
{
    /**
     * Get an id from the current microtime.
     *
     * @return string
     */
    public static function id()
    {
        $snowflake = new Snowflake();

        return $snowflake->id();
    }
}
