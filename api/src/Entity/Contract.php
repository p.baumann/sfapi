<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;


/**
 * @ORM\Table(name="contracts")
 * @ORM\Entity(repositoryClass="App\Repository\ContractRepository")
 */
class Contract
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $legacyId;

    /**
     * @var int
     * @ORM\Column(type="string")
     */
    private $ref_id_typologie_cout;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $type_formule;

    /**
     * @var int
     * @ORM\Column(type="string")
     */
    private $ref_id_def_contrat_remplacement;

    /**
     * @var bool
     * @ORM\Column(type="string")
     */
    private $est_actif_devis;

    /**
     * @var int
     * @ORM\Column(type="string")
     */
    private $age_en_mois_min;

    /**
     * @var int
     * @ORM\Column(type="string")
     */
    private $age_en_mois_max;

    /**
     * Brand constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLegacyId(): int
    {
        return $this->legacyId;
    }

    /**
     * @param int $legacyId
     * @return Contract
     */
    public function setLegacyId(int $legacyId): Contract
    {
        $this->legacyId = $legacyId;
        return $this;
    }

    /**
     * @return int
     */
    public function getRefIdTypologieCout(): int
    {
        return $this->ref_id_typologie_cout;
    }

    /**
     * @param int $ref_id_typologie_cout
     * @return Contract
     */
    public function setRefIdTypologieCout(int $ref_id_typologie_cout): Contract
    {
        $this->ref_id_typologie_cout = $ref_id_typologie_cout;
        return $this;
    }

    /**
     * @return int
     */
    public function getTypeFormule()
    {
        return $this->type_formule;
    }

    /**
     * @param int $type_formule
     * @return Contract
     */
    public function setTypeFormule($type_formule)
    {
        $this->type_formule = $type_formule;
        return $this;
    }

    /**
     * @return int
     */
    public function getRefIdDefContratRemplacement(): int
    {
        return $this->ref_id_def_contrat_remplacement;
    }

    /**
     * @param int $ref_id_def_contrat_remplacement
     * @return Contract
     */
    public function setRefIdDefContratRemplacement(int $ref_id_def_contrat_remplacement): Contract
    {
        $this->ref_id_def_contrat_remplacement = $ref_id_def_contrat_remplacement;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEstActifDevis(): bool
    {
        return $this->est_actif_devis;
    }

    /**
     * @param bool $est_actif_devis
     * @return Contract
     */
    public function setEstActifDevis(bool $est_actif_devis): Contract
    {
        $this->est_actif_devis = $est_actif_devis;
        return $this;
    }

    /**
     * @return int
     */
    public function getAgeEnMoisMin(): int
    {
        return $this->age_en_mois_min;
    }

    /**
     * @param int $age_en_mois_min
     * @return Contract
     */
    public function setAgeEnMoisMin(int $age_en_mois_min): Contract
    {
        $this->age_en_mois_min = $age_en_mois_min;
        return $this;
    }

    /**
     * @return int
     */
    public function getAgeEnMoisMax(): int
    {
        return $this->age_en_mois_max;
    }

    /**
     * @param int $age_en_mois_max
     * @return Contract
     */
    public function setAgeEnMoisMax(int $age_en_mois_max): Contract
    {
        $this->age_en_mois_max = $age_en_mois_max;
        return $this;
    }
}
