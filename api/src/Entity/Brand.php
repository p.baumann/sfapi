<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;


/**
 * @ORM\Table(name="brands")
 * @ORM\Entity(repositoryClass="App\Repository\BrandRepository")
 */
class Brand
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $legacyId;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $nom_abrege;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $tag;

    /**
     * @var bool
     * @ORM\Column(type="string")
     */
    private $utiliser_nom_marital;

    /**
     * @var float
     * @ORM\Column(type="string")
     */
    private $taux_commission;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $couleur_hex_theme;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $couleur_hex_main;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $couleur_hex_main_fonce;

    /**
     * @var bool
     * @ORM\Column(type="string")
     */
    private $autoriser_devis_online;

    /**
     * @var bool
     * @ORM\Column(type="string")
     */
    private $est_actif;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $has_extra_fields;


    /**
     * Brand constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLegacyId(): int
    {
        return $this->legacyId;
    }

    /**
     * @param int $legacyId
     * @return Brand
     */
    public function setLegacyId(int $legacyId): Brand
    {
        $this->legacyId = $legacyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomAbrege(): string
    {
        return $this->nom_abrege;
    }

    /**
     * @param string $nom_abrege
     * @return Brand
     */
    public function setNomAbrege(string $nom_abrege): Brand
    {
        $this->nom_abrege = $nom_abrege;
        return $this;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return Brand
     */
    public function setTag(string $tag): Brand
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUtiliserNomMarital(): bool
    {
        return $this->utiliser_nom_marital;
    }

    /**
     * @param bool $utiliser_nom_marital
     * @return Brand
     */
    public function setUtiliserNomMarital(bool $utiliser_nom_marital): Brand
    {
        $this->utiliser_nom_marital = $utiliser_nom_marital;
        return $this;
    }

    /**
     * @return float
     */
    public function getTauxCommission(): float
    {
        return $this->taux_commission;
    }

    /**
     * @param float $taux_commission
     * @return Brand
     */
    public function setTauxCommission(float $taux_commission): Brand
    {
        $this->taux_commission = $taux_commission;
        return $this;
    }

    /**
     * @return string
     */
    public function getCouleurHexTheme(): string
    {
        return $this->couleur_hex_theme;
    }

    /**
     * @param string $couleur_hex_theme
     * @return Brand
     */
    public function setCouleurHexTheme(string $couleur_hex_theme): Brand
    {
        $this->couleur_hex_theme = $couleur_hex_theme;
        return $this;
    }

    /**
     * @return string
     */
    public function getCouleurHexMain(): string
    {
        return $this->couleur_hex_main;
    }

    /**
     * @param string $couleur_hex_main
     * @return Brand
     */
    public function setCouleurHexMain(string $couleur_hex_main): Brand
    {
        $this->couleur_hex_main = $couleur_hex_main;
        return $this;
    }

    /**
     * @return string
     */
    public function getCouleurHexMainFonce(): string
    {
        return $this->couleur_hex_main_fonce;
    }

    /**
     * @param string $couleur_hex_main_fonce
     * @return Brand
     */
    public function setCouleurHexMainFonce(string $couleur_hex_main_fonce): Brand
    {
        $this->couleur_hex_main_fonce = $couleur_hex_main_fonce;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAutoriserDevisOnline(): bool
    {
        return $this->autoriser_devis_online;
    }

    /**
     * @param bool $autoriser_devis_online
     * @return Brand
     */
    public function setAutoriserDevisOnline(bool $autoriser_devis_online): Brand
    {
        $this->autoriser_devis_online = $autoriser_devis_online;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEstActif(): bool
    {
        return $this->est_actif;
    }

    /**
     * @param bool $est_actif
     * @return Brand
     */
    public function setEstActif(bool $est_actif): Brand
    {
        $this->est_actif = $est_actif;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHasExtraFields(): bool
    {
        return $this->has_extra_fields;
    }

    /**
     * @param bool $has_extra_fields
     * @return Brand
     */
    public function setHasExtraFields(bool $has_extra_fields): Brand
    {
        $this->has_extra_fields = $has_extra_fields;
        return $this;
    }
}
