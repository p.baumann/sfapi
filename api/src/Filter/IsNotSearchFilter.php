<?php

namespace App\Filter;

use ApiPlatform\Core\Api\IdentifiersExtractorInterface;
use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryBuilderHelper;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

/**
 * Class IsNotFilter.
 */
class IsNotSearchFilter extends SearchFilter implements IsNotSearchFilterInterface
{
    // Use to make a difference between the searchFilter
    const PREFIX_FILTER = 'is_not_';

    /**
     * {@inheritdoc}
     */
    public function __construct(
        ManagerRegistry $managerRegistry,
        ?RequestStack $requestStack,
        IriConverterInterface $iriConverter,
        PropertyAccessorInterface $propertyAccessor = null,
        LoggerInterface $logger = null,
        array $properties = null,
        IdentifiersExtractorInterface $identifiersExtractor = null,
        NameConverterInterface $nameConverter = null
    ) {
        parent::__construct(
            $managerRegistry,
            $requestStack,
            $iriConverter,
            $propertyAccessor,
            $logger,
            $properties,
            $identifiersExtractor,
            $nameConverter
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $property = substr($property, strlen(self::PREFIX_FILTER));

        // otherwise filter is applied to order and page as well
        if (\is_null($value) ||
            !$this->isPropertyEnabled($property, $resourceClass) ||
            !$this->isPropertyMapped($property, $resourceClass, true)
        ) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $field = $property;

        $associations = [];
        if ($this->isPropertyNested($property, $resourceClass)) {
            [$alias, $field, $associations] = $this->addJoinsForNestedProperty(
                $property,
                $alias,
                $queryBuilder,
                $queryNameGenerator,
                $resourceClass
            );
        }
        $metadata = $this->getNestedMetadata($resourceClass, $associations);

        $values = $this->normalizeValues((array) $value, $property);
        if (\is_null($values)) {
            return;
        }

        $caseSensitive = true;

        if ($metadata->hasField($field)) {
            if ('id' === $field) {
                $values = array_map([$this, 'getIdFromValue'], $values);
            }

            if (!$this->hasValidValues($values, $this->getDoctrineFieldType($property, $resourceClass))) {
                $this->logger->notice('Invalid filter ignored', [
                    'exception' => new InvalidArgumentException(sprintf(
                        'Values for field "%s" are not valid according to the doctrine type.',
                        $field
                    )),
                ]);

                return;
            }

            $strategy = $this->properties[$property] ?? self::STRATEGY_NOT_EXACT;
            // prefixing the strategy with i makes it case insensitive
            if (0 === strpos($strategy, 'i')) {
                $strategy = substr($strategy, 1);
                $caseSensitive = false;
            }

            if (1 === \count($values)) {
                $this->addWhereByStrategy(
                    $strategy,
                    $queryBuilder,
                    $queryNameGenerator,
                    $alias,
                    $field,
                    $values[0],
                    $caseSensitive
                );

                return;
            }

            if (self::STRATEGY_NOT_EXACT !== $strategy) {
                $this->logger->notice('Invalid filter ignored', [
                    'exception' => new InvalidArgumentException(sprintf(
                        '"%s" strategy selected for "%s" property, but only "%s" strategy supports multiple values',
                        $strategy,
                        $property,
                        self::STRATEGY_NOT_EXACT
                    )),
                ]);

                return;
            }

            $wrapCase = $this->createWrapCase($caseSensitive);
            $valueParameter = $queryNameGenerator->generateParameterName($field);

            $queryBuilder
                ->andWhere(sprintf($wrapCase('%s.%s').' IN (:%s)', $alias, $field, $valueParameter))
                ->setParameter($valueParameter, $caseSensitive ? $values : array_map('strtolower', $values));
        }

        // metadata doesn't have the field, nor an association on the field
        if (!$metadata->hasAssociation($field)) {
            return;
        }

        $values = array_map([$this, 'getIdFromValue'], $values);

        if (!$this->hasValidValues($values, $this->getDoctrineFieldType($property, $resourceClass))) {
            $this->logger->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf(
                    'Values for field "%s" are not valid according to the doctrine type.',
                    $field
                )),
            ]);

            return;
        }

        $association = $field;
        $valueParameter = $queryNameGenerator->generateParameterName($association);

        if ($metadata->isCollectionValuedAssociation($association)) {
            $associationAlias = QueryBuilderHelper::addJoinOnce(
                $queryBuilder,
                $queryNameGenerator,
                $alias,
                $association
            );
            $associationField = 'id';
        } else {
            $associationAlias = $alias;
            $associationField = $field;
        }

        if (1 === \count($values)) {
            $queryBuilder
                ->andWhere(sprintf('%s.%s != :%s', $associationAlias, $associationField, $valueParameter))
                ->setParameter($valueParameter, $values[0]);
        } else {
            $queryBuilder
                ->andWhere(sprintf('%s.%s NOT IN (:%s)', $associationAlias, $associationField, $valueParameter))
                ->setParameter($valueParameter, $values);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function addWhereByStrategy(
        string $strategy,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $alias,
        string $field,
        $value,
        bool $caseSensitive
    ) {
        $wrapCase = $this->createWrapCase($caseSensitive);
        $valueParameter = $queryNameGenerator->generateParameterName($field);
        switch ($strategy) {
            case null:
            case self::STRATEGY_NOT_EXACT:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s').' != '.$wrapCase(':%s'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_NOT_PARTIAL:
                $queryBuilder
                    ->andWhere(sprintf(
                        $wrapCase('%s.%s').' NOT LIKE '.$wrapCase('CONCAT(\'%%\', :%s, \'%%\')'),
                        $alias,
                        $field,
                        $valueParameter
                    ))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_NOT_START:
                $queryBuilder
                    ->andWhere(sprintf(
                        $wrapCase('%s.%s').' NOT LIKE '.$wrapCase('CONCAT(:%s, \'%%\')'),
                        $alias,
                        $field,
                        $valueParameter
                    ))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_NOT_END:
                $queryBuilder
                    ->andWhere(sprintf(
                        $wrapCase('%s.%s').' NOT LIKE '.$wrapCase('CONCAT(\'%%\', :%s)'),
                        $alias,
                        $field,
                        $valueParameter
                    ))
                    ->setParameter($valueParameter, $value);
                break;
            default:
                throw new InvalidArgumentException(sprintf('strategy %s does not exist.', $strategy));
        }
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[self::PREFIX_FILTER.$property] = [
                'property' => $property,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => 'Filter that returns the dataset that is not equal to your value.',
                    'name' => self::PREFIX_FILTER.$property,
                    'type' => 'string',
                ],
            ];
        }

        return $description;
    }
}
