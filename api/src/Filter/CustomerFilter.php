<?php

namespace App\Filter;

use App\Entity\AbstractCustomer;
use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Class CustomerFilter.
 */
final class CustomerFilter extends SQLFilter
{
    /**
     * @param string $targetTableAlias
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (!$targetEntity->getReflectionClass()->isSubclassOf(AbstractCustomer::class)) {
            return '';
        }

        try {
            // Don't worry, getParameter automatically escapes parameters
            $customer = $this->getParameter('customer');
        } catch (\InvalidArgumentException $e) {
            // No user id has been defined
            return '';
        }

        if (empty($customer)) {
            return '';
        }

        return sprintf('%s.%s = %s', $targetTableAlias, $targetEntity->getFieldName('customer'), $customer);
    }
}
