<?php

declare(strict_types=1);

namespace App\Filter;

/**
 * Interface IsNotSearchFilterInterface
 * Interface for filtering the collection by given properties.
 */
interface IsNotSearchFilterInterface
{
    /**
     * @var string Exact matching
     */
    public const STRATEGY_NOT_EXACT = 'not_exact';

    /**
     * @var string The value must be contained in the field
     */
    public const STRATEGY_NOT_PARTIAL = 'not_partial';

    /**
     * @var string Finds fields that are starting with the value
     */
    public const STRATEGY_NOT_START = 'not_start';

    /**
     * @var string Finds fields that are ending with the value
     */
    public const STRATEGY_NOT_END = 'not_end';
}
