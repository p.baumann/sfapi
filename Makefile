DOCKER_COMPOSE  		= docker-compose

EXEC_PHP        		= $(DOCKER_COMPOSE) exec -T php /entrypoint
EXEC_JS         		= $(DOCKER_COMPOSE) exec -T node /entrypoint

SYMFONY         		= $(EXEC_PHP) bin/console
COMPOSER        		= $(DOCKER_COMPOSE) exec -e COMPOSER_MEMORY_LIMIT=-1 -T php /entrypoint composer
YARN            		= $(EXEC_JS) yarn

SOURCE_BRANCH 			= $(shell git rev-parse --abbrev-ref HEAD)
TARGET_BRANCH 			= develop
BRANCH_BASE_COMMIT	= $(shell git merge-base $(SOURCE_BRANCH) $(TARGET_BRANCH))
COMMIT_RANGE 				= "$(BRANCH_BASE_COMMIT)..$(SOURCE_BRANCH)"
CHANGED_FILES 			= $(shell git diff --name-only --diff-filter=ACMRTUXB $(COMMIT_RANGE) -- '*.php' '*.ctp')
REVERSE_PROXY_IP 		= $(shell docker network inspect -f '{{ index (index .IPAM.Config) 0 "Gateway" }}' web-proxy)

build:
	@$(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull

migration:
	docker-compose exec php bin/console doctrine:migrations:generate

migrations:
	docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction

update:
	docker-compose exec php bin/console doctrine:schema:update --force

db:
	docker-compose exec php bin/console doctrine:database:drop --if-exists --force
	docker-compose exec php bin/console doctrine:database:create --if-not-exists
	#docker-compose exec php bin/console doctrine:schema:create --no-interaction

fixtures:
	docker-compose exec php bin/console hautelook:fixtures:load --no-interaction

diff:
	docker-compose exec php bin/console doctrine:migrations:diff

cc:
	docker-compose exec php bin/console ca:cl

fpm:
	docker-compose exec php php-fpm -D

up:
	docker-compose up -d

start: up db migrations fixtures


